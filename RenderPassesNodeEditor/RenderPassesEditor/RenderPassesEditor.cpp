# include <ax/Widgets.h>
# include <iostream>
# include "Application.h"

# include "NodeModelsFunctions.h"
# include "NodeModel.h"
# include <imgui_internal.h>

static inline ImRect ImGui_GetItemRect()
{
	return ImRect(ImGui::GetItemRectMin(), ImGui::GetItemRectMax());
}

static inline ImRect ImRect_Expanded(const ImRect &rect, float x, float y)
{
	auto result = rect;
	result.Min.x -= x;
	result.Min.y -= y;
	result.Max.x += x;
	result.Max.y += y;
	return result;
}

namespace ed   = ax::NodeEditor;
namespace util = ax::NodeEditor::Utilities;

using namespace ax;

using ax::Widgets::IconType;

static ed::EditorContext *m_Editor = nullptr;

static const int s_PinIconSize = 24;
//static std::vector<Node> s_Nodes;
static std::vector<Link> s_Links;
static ImTextureID s_HeaderBackground = nullptr;
// static ImTextureID          s_SampleImage = nullptr;
static ImTextureID s_SaveIcon    = nullptr;
static ImTextureID s_RestoreIcon = nullptr;

struct NodeIdComparator {
	std::string compType = "VOID";
	// Heritage compliqué avec une structure abstraite dans la map
	bool operator()(const ed::NodeId &lhs, const ed::NodeId &rhs) const
	{
		return true;
	}
};

struct NodeIdLower : NodeIdComparator {
	std::string compType = "LOWER";
	bool operator()(const ed::NodeId &lhs, const ed::NodeId &rhs) const
	{
		return lhs.AsPointer() < rhs.AsPointer();
	}
};

struct NodeIdGreater : NodeIdComparator {
	std::string compType = "GREATER";
	bool operator()(const ed::NodeId &lhs, const ed::NodeId &rhs) const
	{
		return lhs.AsPointer() > rhs.AsPointer();
	}
};



static const float s_TouchTime = 1.0f;
static std::map<ed::NodeId, float, NodeIdComparator> s_NodeTouchTime;


static ed::LinkId GetNextLinkId()
{
	return ed::LinkId(GetNextId());
}

static void TouchNode(ed::NodeId id)
{
	s_NodeTouchTime[id] = s_TouchTime;
}

static float GetTouchProgress(ed::NodeId id)
{
	auto it = s_NodeTouchTime.find(id);
	if (it != s_NodeTouchTime.end() && it->second > 0.0f)
		return (s_TouchTime - it->second) / s_TouchTime;
	else
		return 0.0f;
}

static void UpdateTouch()
{
	const auto deltaTime = ImGui::GetIO().DeltaTime;
	for (auto &entry : s_NodeTouchTime) {
		if (entry.second > 0.0f)
			entry.second -= deltaTime;
	}
}

static Node *FindNode(ed::NodeId id)
{
	for (auto &node : s_Nodes)
		if (node.ID == id)
			return &node;

	return nullptr;
}

static Link *FindLink(ed::LinkId id)
{
	for (auto &link : s_Links)
		if (link.ID == id)
			return &link;

	return nullptr;
}

static Pin *FindPin(ed::PinId id)
{
	if (!id)
		return nullptr;

	for (auto &node : s_Nodes) {
		for (auto &pin : node.Inputs)
			if (pin.ID == id)
				return &pin;

		for (auto &pin : node.Outputs)
			if (pin.ID == id)
				return &pin;
	}

	return nullptr;
}

static bool IsPinLinked(ed::PinId id)
{
	if (!id)
		return false;

	for (auto &link : s_Links)
		if (link.StartPinID == id || link.EndPinID == id)
			return true;

	return false;
}

// Indicates if we can create links between two type
// Says no if pins are null, or differents types 
// OR from Output to Output, from Inputs to Inputs 
// YES : except for FloatManagers out -LinkedTo- Floats in
// YES : except for  ColorManagers out -LinkedTo- Framebuffer in
static bool CanCreateLink(Pin *a, Pin *b)
{
	if  ( ( !a || !b || a == b || a->Kind == b->Kind || a->Type != b->Type || a->Node == b->Node ) &&

		( ! (a->Type == PinType::FloatManager && b->Type == PinType::Float) || a->Kind == b->Kind || a->Node == b->Node ) &&
		( ! (b->Type == PinType::FloatManager && a->Type == PinType::Float) || a->Kind == b->Kind || a->Node == b->Node ) &&

		( ! (a->Type == PinType::ColorManager && b->Type == PinType::Framebuffer) || a->Kind == b->Kind || a->Node == b->Node) &&
		( ! (b->Type == PinType::ColorManager && a->Type == PinType::Framebuffer) || a->Kind == b->Kind || a->Node == b->Node) ) 
		
		return false;

	return true;
}

const char *Application_GetName()
{
	return "RenderPasses";
}

void Application_Initialize()
{
	ed::Config config;

	config.SettingsFile = "RenderPasses.json";

	config.LoadNodeSettings = [](ed::NodeId nodeId, char *data, void *userPointer) -> size_t {
		auto node = FindNode(nodeId);
		if (!node)
			return 0;

		if (data != nullptr)
			memcpy(data, node->State.data(), node->State.size());
		return node->State.size();
	};

	config.SaveNodeSettings =
		[](ed::NodeId nodeId, const char *data, size_t size, ed::SaveReasonFlags reason, void *userPointer) -> bool {
		auto node = FindNode(nodeId);
		if (!node)
			return false;

		node->State.assign(data, size);

		TouchNode(nodeId);

		return true;
	};

	m_Editor = ed::CreateEditor(&config);
	ed::SetCurrentEditor(m_Editor);

	BuildNodes();

	s_HeaderBackground = Application_LoadTexture("Data/BlueprintBackground.png");
	s_SaveIcon         = Application_LoadTexture("Data/ic_save_white_24dp.png");
	s_RestoreIcon      = Application_LoadTexture("Data/ic_restore_white_24dp.png");

	// auto& io = ImGui::GetIO();
}

void Application_Finalize()
{
	auto releaseTexture = [](ImTextureID &id) {
		if (id) {
			Application_DestroyTexture(id);
			id = nullptr;
		}
	};

	releaseTexture(s_RestoreIcon);
	releaseTexture(s_SaveIcon);
	releaseTexture(s_HeaderBackground);

	if (m_Editor) {
		ed::DestroyEditor(m_Editor);
		m_Editor = nullptr;
	}
}

static bool Splitter(bool split_vertically,
					 float thickness,
					 float *size1,
					 float *size2,
					 float min_size1,
					 float min_size2,
					 float splitter_long_axis_size = -1.0f)
{
	using namespace ImGui;
	ImGuiContext &g     = *GImGui;
	ImGuiWindow *window = g.CurrentWindow;
	ImGuiID id          = window->GetID("##Splitter");
	ImRect bb;
	bb.Min = window->DC.CursorPos + (split_vertically ? ImVec2(*size1, 0.0f) : ImVec2(0.0f, *size1));
	bb.Max = bb.Min + CalcItemSize(split_vertically ? ImVec2(thickness, splitter_long_axis_size)
													: ImVec2(splitter_long_axis_size, thickness),
								   0.0f,
								   0.0f);
	return SplitterBehavior(bb, id, split_vertically ? ImGuiAxis_X : ImGuiAxis_Y, size1, size2, min_size1, min_size2, 0.0f);
}

ImColor GetIconColor(PinType type)
{
	switch (type) {
	default:
		//        case PinType::Flow:     return ImColor(255, 255, 255);
	case PinType::Bool:
		return ImColor(220, 48, 48);
	case PinType::Int:
		return ImColor(68, 201, 156);
	}
};

// Pin icons management
void DrawPinIcon(const Pin &pin, bool connected, int alpha)
{
	IconType iconType;
	ImColor color = GetIconColor(pin.Type);
	color.Value.w = alpha / 255.0f;
	switch (pin.Type) {
		//        case PinType::Flow:     iconType = IconType::Flow;   break;
	case PinType::Bool:
		iconType = IconType::Circle;
		break;

	case PinType::Int:
		iconType = IconType::Circle;
		break;

	case PinType::Float  :
		iconType = IconType::Circle;
		break;

	case PinType::FloatManager  :
		iconType = IconType::Circle;
		break;

	case PinType::ColorManager :
		iconType = IconType::Circle;
		break;

		//        case PinType::String:   iconType = IconType::Circle; break;
		//        case PinType::Object:   iconType = IconType::Circle; break;
		//        case PinType::Function: iconType = IconType::Circle; break;
		//        case PinType::Delegate: iconType = IconType::Square; break;
	case PinType::Scene:
		iconType = IconType::Square;
		break;

	case PinType::Framebuffer:
		iconType = IconType::Square;
		break;

	case PinType::Texture:
		iconType = IconType::Square;
		break;

	default:
		return;
	}

	ax::Widgets::Icon(ImVec2(s_PinIconSize, s_PinIconSize), iconType, connected, color, ImColor(32, 32, 32, alpha));
};

void ShowStyleEditor(bool *show = nullptr)
{
	if (!ImGui::Begin("Style", show)) {
		ImGui::End();
		return;
	}

	auto paneWidth = ImGui::GetContentRegionAvailWidth();

	auto &editorStyle = ed::GetStyle();
	ImGui::BeginHorizontal("Style buttons", ImVec2(paneWidth, 0), 1.0f);
	ImGui::TextUnformatted("Values");
	ImGui::Spring();
	if (ImGui::Button("Reset to defaults"))
		editorStyle = ed::Style();
	ImGui::EndHorizontal();
	ImGui::Spacing();
	ImGui::DragFloat4("Node Padding", &editorStyle.NodePadding.x, 0.1f, 0.0f, 40.0f);
	ImGui::DragFloat("Node Rounding", &editorStyle.NodeRounding, 0.1f, 0.0f, 40.0f);
	ImGui::DragFloat("Node Border Width", &editorStyle.NodeBorderWidth, 0.1f, 0.0f, 15.0f);
	ImGui::DragFloat("Hovered Node Border Width", &editorStyle.HoveredNodeBorderWidth, 0.1f, 0.0f, 15.0f);
	ImGui::DragFloat("Selected Node Border Width", &editorStyle.SelectedNodeBorderWidth, 0.1f, 0.0f, 15.0f);
	ImGui::DragFloat("Pin Rounding", &editorStyle.PinRounding, 0.1f, 0.0f, 40.0f);
	ImGui::DragFloat("Pin Border Width", &editorStyle.PinBorderWidth, 0.1f, 0.0f, 15.0f);
	ImGui::DragFloat("Link Strength", &editorStyle.LinkStrength, 1.0f, 0.0f, 500.0f);
	ImGui::DragFloat("Scroll Duration", &editorStyle.ScrollDuration, 0.001f, 0.0f, 2.0f);
	ImGui::DragFloat("Flow Marker Distance", &editorStyle.FlowMarkerDistance, 1.0f, 1.0f, 200.0f);
	ImGui::DragFloat("Flow Speed", &editorStyle.FlowSpeed, 1.0f, 1.0f, 2000.0f);
	ImGui::DragFloat("Flow Duration", &editorStyle.FlowDuration, 0.001f, 0.0f, 5.0f);
	ImGui::DragFloat("Group Rounding", &editorStyle.GroupRounding, 0.1f, 0.0f, 40.0f);
	ImGui::DragFloat("Group Border Width", &editorStyle.GroupBorderWidth, 0.1f, 0.0f, 15.0f);

	ImGui::Separator();

	static ImGuiColorEditFlags edit_mode = ImGuiColorEditFlags_RGB;
	ImGui::BeginHorizontal("Color Mode", ImVec2(paneWidth, 0), 1.0f);
	ImGui::TextUnformatted("Filter Colors");
	ImGui::Spring();
	ImGui::RadioButton("RGB", &edit_mode, ImGuiColorEditFlags_RGB);
	ImGui::Spring(0);
	ImGui::RadioButton("HSV", &edit_mode, ImGuiColorEditFlags_HSV);
	ImGui::Spring(0);
	ImGui::RadioButton("HEX", &edit_mode, ImGuiColorEditFlags_HEX);
	ImGui::EndHorizontal();

	static ImGuiTextFilter filter;
	filter.Draw("", paneWidth);

	ImGui::Spacing();

	ImGui::PushItemWidth(-160);
	for (int i = 0; i < ed::StyleColor_Count; ++i) {
		auto name = ed::GetStyleColorName((ed::StyleColor)i);
		if (!filter.PassFilter(name))
			continue;

		ImGui::ColorEdit4(name, &editorStyle.Colors[i].x, edit_mode);
	}
	ImGui::PopItemWidth();

	ImGui::End();
}

void ShowLeftPane(float paneWidth)
{
	auto &io = ImGui::GetIO();

	ImGui::BeginChild("Selection", ImVec2(paneWidth, 0));

	paneWidth = ImGui::GetContentRegionAvailWidth();

	static bool showStyleEditor = false;
	ImGui::BeginHorizontal("Style Editor", ImVec2(paneWidth, 0));
	ImGui::Spring(0.0f, 0.0f);
	if (ImGui::Button("Zoom to Content"))
		ed::NavigateToContent();
	ImGui::Spring(0.0f);
	if (ImGui::Button("Show Flow")) {
		for (auto &link : s_Links)
			ed::Flow(link.ID);
	}
	ImGui::Spring();
	if (ImGui::Button("Edit Style"))
		showStyleEditor = true;
	ImGui::EndHorizontal();

	if (showStyleEditor)
		ShowStyleEditor(&showStyleEditor);

	std::vector<ed::NodeId> selectedNodes;
	std::vector<ed::LinkId> selectedLinks;
	selectedNodes.resize(ed::GetSelectedObjectCount());
	selectedLinks.resize(ed::GetSelectedObjectCount());

	int nodeCount = ed::GetSelectedNodes(selectedNodes.data(), static_cast<int>(selectedNodes.size()));
	int linkCount = ed::GetSelectedLinks(selectedLinks.data(), static_cast<int>(selectedLinks.size()));

	selectedNodes.resize(nodeCount);
	selectedLinks.resize(linkCount);

	int saveIconWidth     = Application_GetTextureWidth(s_SaveIcon);
	int saveIconHeight    = Application_GetTextureWidth(s_SaveIcon);
	int restoreIconWidth  = Application_GetTextureWidth(s_RestoreIcon);
	int restoreIconHeight = Application_GetTextureWidth(s_RestoreIcon);

	ImGui::GetWindowDrawList()->AddRectFilled(ImGui::GetCursorScreenPos(),
											  ImGui::GetCursorScreenPos() + ImVec2(paneWidth, ImGui::GetTextLineHeight()),
											  ImColor(ImGui::GetStyle().Colors[ImGuiCol_HeaderActive]),
											  ImGui::GetTextLineHeight() * 0.25f);
	ImGui::Spacing();
	ImGui::SameLine();
	ImGui::TextUnformatted("Nodes");
	ImGui::Indent();
	for (auto &node : s_Nodes) {
		ImGui::PushID(node.ID.AsPointer());
		auto start = ImGui::GetCursorScreenPos();

		if (const auto progress = GetTouchProgress(node.ID)) {
			ImGui::GetWindowDrawList()->AddLine(start + ImVec2(-8, 0),
												start + ImVec2(-8, ImGui::GetTextLineHeight()),
												IM_COL32(255, 0, 0, 255 - (int)(255 * progress)),
												4.0f);
		}

		bool isSelected = std::find(selectedNodes.begin(), selectedNodes.end(), node.ID) != selectedNodes.end();
		if (ImGui::Selectable((node.Name + "##" + std::to_string(reinterpret_cast<uintptr_t>(node.ID.AsPointer()))).c_str(),
							  &isSelected)) {
			if (io.KeyCtrl) {
				if (isSelected)
					ed::SelectNode(node.ID, true);
				else
					ed::DeselectNode(node.ID);
			} else
				ed::SelectNode(node.ID, false);

			ed::NavigateToSelection();
		}
		if (ImGui::IsItemHovered() && !node.State.empty())
			ImGui::SetTooltip("State: %s", node.State.c_str());

		auto id           = std::string("(") + std::to_string(reinterpret_cast<uintptr_t>(node.ID.AsPointer())) + ")";
		auto textSize     = ImGui::CalcTextSize(id.c_str(), nullptr);
		auto iconPanelPos = start + ImVec2(paneWidth - ImGui::GetStyle().FramePadding.x - ImGui::GetStyle().IndentSpacing -
											   saveIconWidth - restoreIconWidth - ImGui::GetStyle().ItemInnerSpacing.x * 1,
										   (ImGui::GetTextLineHeight() - saveIconHeight) / 2);
		ImGui::GetWindowDrawList()->AddText(ImVec2(iconPanelPos.x - textSize.x - ImGui::GetStyle().ItemInnerSpacing.x, start.y),
											IM_COL32(255, 255, 255, 255),
											id.c_str(),
											nullptr);

		auto drawList = ImGui::GetWindowDrawList();
		ImGui::SetCursorScreenPos(iconPanelPos);
		ImGui::SetItemAllowOverlap();
		if (node.SavedState.empty()) {
			if (ImGui::InvisibleButton("save", ImVec2((float)saveIconWidth, (float)saveIconHeight)))
				node.SavedState = node.State;

			if (ImGui::IsItemActive())
				drawList->AddImage(s_SaveIcon,
								   ImGui::GetItemRectMin(),
								   ImGui::GetItemRectMax(),
								   ImVec2(0, 0),
								   ImVec2(1, 1),
								   IM_COL32(255, 255, 255, 96));
			else if (ImGui::IsItemHovered())
				drawList->AddImage(s_SaveIcon,
								   ImGui::GetItemRectMin(),
								   ImGui::GetItemRectMax(),
								   ImVec2(0, 0),
								   ImVec2(1, 1),
								   IM_COL32(255, 255, 255, 255));
			else
				drawList->AddImage(s_SaveIcon,
								   ImGui::GetItemRectMin(),
								   ImGui::GetItemRectMax(),
								   ImVec2(0, 0),
								   ImVec2(1, 1),
								   IM_COL32(255, 255, 255, 160));
		} else {
			ImGui::Dummy(ImVec2((float)saveIconWidth, (float)saveIconHeight));
			drawList->AddImage(s_SaveIcon,
							   ImGui::GetItemRectMin(),
							   ImGui::GetItemRectMax(),
							   ImVec2(0, 0),
							   ImVec2(1, 1),
							   IM_COL32(255, 255, 255, 32));
		}

		ImGui::SameLine(0, ImGui::GetStyle().ItemInnerSpacing.x);
		ImGui::SetItemAllowOverlap();
		if (!node.SavedState.empty()) {
			if (ImGui::InvisibleButton("restore", ImVec2((float)restoreIconWidth, (float)restoreIconHeight))) {
				node.State = node.SavedState;
				ed::RestoreNodeState(node.ID);
				node.SavedState.clear();
			}

			if (ImGui::IsItemActive())
				drawList->AddImage(s_RestoreIcon,
								   ImGui::GetItemRectMin(),
								   ImGui::GetItemRectMax(),
								   ImVec2(0, 0),
								   ImVec2(1, 1),
								   IM_COL32(255, 255, 255, 96));
			else if (ImGui::IsItemHovered())
				drawList->AddImage(s_RestoreIcon,
								   ImGui::GetItemRectMin(),
								   ImGui::GetItemRectMax(),
								   ImVec2(0, 0),
								   ImVec2(1, 1),
								   IM_COL32(255, 255, 255, 255));
			else
				drawList->AddImage(s_RestoreIcon,
								   ImGui::GetItemRectMin(),
								   ImGui::GetItemRectMax(),
								   ImVec2(0, 0),
								   ImVec2(1, 1),
								   IM_COL32(255, 255, 255, 160));
		} else {
			ImGui::Dummy(ImVec2((float)restoreIconWidth, (float)restoreIconHeight));
			drawList->AddImage(s_RestoreIcon,
							   ImGui::GetItemRectMin(),
							   ImGui::GetItemRectMax(),
							   ImVec2(0, 0),
							   ImVec2(1, 1),
							   IM_COL32(255, 255, 255, 32));
		}

		ImGui::SameLine(0, 0);
		ImGui::SetItemAllowOverlap();
		ImGui::Dummy(ImVec2(0, (float)restoreIconHeight));

		ImGui::PopID();
	}
	ImGui::Unindent();

	static int changeCount = 0;

	ImGui::GetWindowDrawList()->AddRectFilled(ImGui::GetCursorScreenPos(),
											  ImGui::GetCursorScreenPos() + ImVec2(paneWidth, ImGui::GetTextLineHeight()),
											  ImColor(ImGui::GetStyle().Colors[ImGuiCol_HeaderActive]),
											  ImGui::GetTextLineHeight() * 0.25f);
	ImGui::Spacing();
	ImGui::SameLine();
	ImGui::TextUnformatted("Selection");

	ImGui::BeginHorizontal("Selection Stats", ImVec2(paneWidth, 0));
	ImGui::Text("Changed %d time%s", changeCount, changeCount > 1 ? "s" : "");
	ImGui::Spring();
	if (ImGui::Button("Deselect All"))
		ed::ClearSelection();
	ImGui::EndHorizontal();
	ImGui::Indent();
	for (int i = 0; i < nodeCount; ++i)
		ImGui::Text("Node (%p)", selectedNodes[i].AsPointer());
	for (int i = 0; i < linkCount; ++i)
		ImGui::Text("Link (%p)", selectedLinks[i].AsPointer());
	ImGui::Unindent();

	if (ImGui::IsKeyPressed(ImGui::GetKeyIndex(ImGuiKey_Z)))
		for (auto &link : s_Links)
			ed::Flow(link.ID);

	if (ed::HasSelectionChanged())
		++changeCount;

	ImGui::EndChild();
}

void Application_Frame()
{
	UpdateTouch();

	auto &io = ImGui::GetIO();

	ImGui::Text("FPS: %.2f (%.2gms)", io.Framerate, io.Framerate ? 1000.0f / io.Framerate : 0.0f);

	ed::SetCurrentEditor(m_Editor);

	// auto& style = ImGui::GetStyle();

#if 0
	{
		for (auto x = -io.DisplaySize.y; x < io.DisplaySize.x; x += 10.0f)
		{
			ImGui::GetWindowDrawList()->AddLine(ImVec2(x, 0), ImVec2(x + io.DisplaySize.y, io.DisplaySize.y),
				IM_COL32(255, 255, 0, 255));
		}
	}
#endif

	static ed::NodeId contextNodeId = 0;
	static ed::LinkId contextLinkId = 0;
	static ed::PinId contextPinId   = 0;
	static bool createNewNode       = false;
	static Pin *newNodeLinkPin      = nullptr;
	static Pin *newLinkPin          = nullptr;

	static float leftPaneWidth  = 400.0f;
	static float rightPaneWidth = 800.0f;
	Splitter(true, 4.0f, &leftPaneWidth, &rightPaneWidth, 50.0f, 50.0f);

	ShowLeftPane(leftPaneWidth - 4.0f);

	ImGui::SameLine(0.0f, 12.0f);

	ed::Begin("Node editor");
	{
		auto cursorTopLeft = ImGui::GetCursorScreenPos();

		util::BlueprintNodeBuilder builder(s_HeaderBackground,
										   Application_GetTextureWidth(s_HeaderBackground),
										   Application_GetTextureHeight(s_HeaderBackground));

		for (auto &node : s_Nodes) {
			if (node.Type != NodeType::Float)
				continue;

			const float commentAlpha = 0.75f;

			ImGui::PushStyleVar(ImGuiStyleVar_Alpha, commentAlpha);
			ed::PushStyleColor(ed::StyleColor_NodeBg, ImColor(255, 255, 255, 64));
			ed::PushStyleColor(ed::StyleColor_NodeBorder, ImColor(255, 255, 255, 64));
			ed::BeginNode(node.ID);
			ImGui::PushID(node.ID.AsPointer());
			ImGui::BeginVertical("content");
			ImGui::BeginHorizontal("horizontal");
			ImGui::Spring(1);
			ImGui::TextUnformatted(node.Name.c_str());
			ImGui::Spring(1);
			ImGui::EndHorizontal();


			// ICI, Je n'arrive a faire afficher le slider dans ce noeud comme ca
			
			/*for (auto slider : node.FloatCTRL) {
				ImGui::Spring(1);
				ImGui::TextUnformatted(slider.Name.c_str());
				ImGui::Text("[%f -- %f -- %f]", slider.Min, slider.Value, slider.Max);
				 // create a slider
				ImGui::Spring(1);
			}*/

			ed::Group(node.Size);
			ImGui::EndVertical();
			ImGui::PopID();

			for (auto &output : node.Outputs) {
				// create output pins
			}

			ed::EndNode();
			ed::PopStyleColor(2);
			ImGui::PopStyleVar();

			if (ed::BeginGroupHint(node.ID)) {
				// auto alpha   = static_cast<int>(commentAlpha * ImGui::GetStyle().Alpha * 255);
				auto bgAlpha = static_cast<int>(ImGui::GetStyle().Alpha * 255);

				// ImGui::PushStyleVar(ImGuiStyleVar_Alpha, commentAlpha * ImGui::GetStyle().Alpha);

				auto min = ed::GetGroupMin();
				// auto max = ed::GetGroupMax();

				ImGui::SetCursorScreenPos(min - ImVec2(-8, ImGui::GetTextLineHeightWithSpacing() + 4));
				ImGui::BeginGroup();
				ImGui::TextUnformatted(node.Name.c_str());
				ImGui::EndGroup();

				auto drawList = ed::GetHintBackgroundDrawList();

				auto hintBounds      = ImGui_GetItemRect();
				auto hintFrameBounds = ImRect_Expanded(hintBounds, 8, 4);

				drawList->AddRectFilled(
					hintFrameBounds.GetTL(), hintFrameBounds.GetBR(), IM_COL32(255, 255, 255, 64 * bgAlpha / 255), 4.0f);

				drawList->AddRect(
					hintFrameBounds.GetTL(), hintFrameBounds.GetBR(), IM_COL32(255, 255, 255, 128 * bgAlpha / 255), 4.0f);

				// ImGui::PopStyleVar();
			}
			ed::EndGroupHint();
		}


		// Displays Nodes
		for (auto &node : s_Nodes) {
			if (node.Type != NodeType::Blueprint && node.Type != NodeType::Simple)
				continue;

			const auto isSimple = node.Type == NodeType::Simple;

			//    bool hasOutputDelegates = false;
			// for (auto& output : node.Outputs)
			//     if (output.Type == PinType::Delegate)
			//  hasOutputDelegates = true;

			builder.Begin(node.ID);
			if (!isSimple) {
				builder.Header(node.Color);
				ImGui::Spring(0);
				ImGui::TextUnformatted(node.Name.c_str());
				ImGui::Spring(1);
				ImGui::Dummy(ImVec2(0, 28));
				ImGui::Spring(0);
				builder.EndHeader();
			}

			//Displays inputs pins

			for (auto &input : node.Inputs) {
				auto alpha = ImGui::GetStyle().Alpha;
				if (newLinkPin && !CanCreateLink(newLinkPin, &input) && &input != newLinkPin)
					alpha = alpha * (48.0f / 255.0f);

				builder.Input(input.ID);
				ImGui::PushStyleVar(ImGuiStyleVar_Alpha, alpha);
				DrawPinIcon(input, IsPinLinked(input.ID), (int)(alpha * 255));
				ImGui::Spring(0);
				if (!input.Name.empty()) {
					ImGui::TextUnformatted(input.Name.c_str());
					ImGui::Spring(0);
				}
				if (input.Type == PinType::Bool) {
					ImGui::Button("Hello");
					ImGui::Spring(0);
				}
				ImGui::PopStyleVar();
				builder.EndInput();
			}

			if (isSimple) {
				builder.Middle();

				ImGui::Spring(1, 0);
				ImGui::TextUnformatted(node.Name.c_str());
				ImGui::Spring(1, 0);
			}

			// Displays output pins

			for (auto &output : node.Outputs) {
				auto alpha = ImGui::GetStyle().Alpha;
				if (newLinkPin && !CanCreateLink(newLinkPin, &output) && &output != newLinkPin)
					alpha = alpha * (48.0f / 255.0f);

				ImGui::PushStyleVar(ImGuiStyleVar_Alpha, alpha);
				builder.Output(output.ID);

				if (!output.Name.empty()) {
					ImGui::Spring(0);
					ImGui::TextUnformatted(output.Name.c_str());
				}

				// ICI Output FLOATMANAGER implique gestion des FLOATCONTROLLER
				// Display sliders for FloatManager
				if (output.Type == PinType::FloatManager)
				{

					for (auto &slider : node.floatModel){
						ImGui::PushItemWidth(100.0f);
						ImGui::SliderFloat("Float", &slider.Value, slider.Min, slider.Max);
						ImGui::NewLine();
						ImGui::PopItemWidth();
					}
				}

				// ICI Output ColorManager pin implique gestion du ColorPicker
				// Display color edit for ColorManager pins
				else if( output.Type == PinType::ColorManager) {
						ImGui::PushItemWidth(100.0f);
						// ImGui::ColorEdit4("Color Edit", &node.ColorPicker.x);
						ImGui::ColorPicker4("Output\nColor\nPicker", &node.ColorPicker.x);
				}


				ImGui::Spring(0);
				DrawPinIcon(output, IsPinLinked(output.ID), (int)(alpha * 255));
				ImGui::PopStyleVar();
				builder.EndOutput();
			}

			builder.End();
		}

		for (auto &node : s_Nodes) {
			if (node.Type != NodeType::Tree)
				continue;

			const float rounding = 5.0f;
			const float padding  = 12.0f;

			const auto pinBackground = ed::GetStyle().Colors[ed::StyleColor_NodeBg];

			ed::PushStyleColor(ed::StyleColor_NodeBg, ImColor(128, 128, 128, 200));
			ed::PushStyleColor(ed::StyleColor_NodeBorder, ImColor(32, 32, 32, 200));
			ed::PushStyleColor(ed::StyleColor_PinRect, ImColor(60, 180, 255, 150));
			ed::PushStyleColor(ed::StyleColor_PinRectBorder, ImColor(60, 180, 255, 150));

			ed::PushStyleVar(ed::StyleVar_NodePadding, ImVec4(0, 0, 0, 0));
			ed::PushStyleVar(ed::StyleVar_NodeRounding, rounding);
			ed::PushStyleVar(ed::StyleVar_SourceDirection, ImVec2(0.0f, 1.0f));
			ed::PushStyleVar(ed::StyleVar_TargetDirection, ImVec2(0.0f, -1.0f));
			ed::PushStyleVar(ed::StyleVar_LinkStrength, 0.0f);
			ed::PushStyleVar(ed::StyleVar_PinBorderWidth, 1.0f);
			ed::PushStyleVar(ed::StyleVar_PinRadius, 5.0f);
			ed::BeginNode(node.ID);

			ImGui::BeginVertical(node.ID.AsPointer());
			ImGui::BeginHorizontal("inputs");
			ImGui::Spring(0, padding * 2);

			ImRect inputsRect;
			int inputAlpha = 200;
			if (!node.Inputs.empty()) {
				auto &pin = node.Inputs[0];
				ImGui::Dummy(ImVec2(0, padding));
				ImGui::Spring(1, 0);
				inputsRect = ImGui_GetItemRect();

				ed::PushStyleVar(ed::StyleVar_PinArrowSize, 10.0f);
				ed::PushStyleVar(ed::StyleVar_PinArrowWidth, 10.0f);
				ed::PushStyleVar(ed::StyleVar_PinCorners, 12);
				ed::BeginPin(pin.ID, ed::PinKind::Input);
				ed::PinPivotRect(inputsRect.GetTL(), inputsRect.GetBR());
				ed::PinRect(inputsRect.GetTL(), inputsRect.GetBR());
				ed::EndPin();
				ed::PopStyleVar(3);

				if (newLinkPin && !CanCreateLink(newLinkPin, &pin) && &pin != newLinkPin)
					inputAlpha = (int)(255 * ImGui::GetStyle().Alpha * (48.0f / 255.0f));
			} else
				ImGui::Dummy(ImVec2(0, padding));

			ImGui::Spring(0, padding * 2);
			ImGui::EndHorizontal();

			ImGui::BeginHorizontal("content_frame");
			ImGui::Spring(1, padding);

			ImGui::BeginVertical("content", ImVec2(0.0f, 0.0f));
			ImGui::Dummy(ImVec2(160, 0));
			ImGui::Spring(1);
			ImGui::TextUnformatted(node.Name.c_str());
			ImGui::Spring(1);
			ImGui::EndVertical();
			auto contentRect = ImGui_GetItemRect();

			ImGui::Spring(1, padding);
			ImGui::EndHorizontal();

			ImGui::BeginHorizontal("outputs");
			ImGui::Spring(0, padding * 2);

			ImRect outputsRect;
			int outputAlpha = 200;
			if (!node.Outputs.empty()) {
				auto &pin = node.Outputs[0];
				ImGui::Dummy(ImVec2(0, padding));
				ImGui::Spring(1, 0);
				outputsRect = ImGui_GetItemRect();

				ed::PushStyleVar(ed::StyleVar_PinCorners, 3);
				ed::BeginPin(pin.ID, ed::PinKind::Output);
				ed::PinPivotRect(outputsRect.GetTL(), outputsRect.GetBR());
				ed::PinRect(outputsRect.GetTL(), outputsRect.GetBR());
				ed::EndPin();
				ed::PopStyleVar();

				if (newLinkPin && !CanCreateLink(newLinkPin, &pin) && &pin != newLinkPin)
					outputAlpha = (int)(255 * ImGui::GetStyle().Alpha * (48.0f / 255.0f));
			} else
				ImGui::Dummy(ImVec2(0, padding));

			ImGui::Spring(0, padding * 2);
			ImGui::EndHorizontal();

			ImGui::EndVertical();

			ed::EndNode();
			ed::PopStyleVar(7);
			ed::PopStyleColor(4);

			auto drawList = ed::GetNodeBackgroundDrawList(node.ID);
			drawList->AddRectFilled(
				inputsRect.GetTL() + ImVec2(0, 1),
				inputsRect.GetBR(),
				IM_COL32((int)(255 * pinBackground.x), (int)(255 * pinBackground.y), (int)(255 * pinBackground.z), inputAlpha),
				4.0f,
				12);
			// ImGui::PushStyleVar(ImGuiStyleVar_AntiAliasFringeScale, 1.0f);
			drawList->AddRect(
				inputsRect.GetTL() + ImVec2(0, 1),
				inputsRect.GetBR(),
				IM_COL32((int)(255 * pinBackground.x), (int)(255 * pinBackground.y), (int)(255 * pinBackground.z), inputAlpha),
				4.0f,
				12);
			// ImGui::PopStyleVar();
			drawList->AddRectFilled(
				outputsRect.GetTL(),
				outputsRect.GetBR() - ImVec2(0, 1),
				IM_COL32((int)(255 * pinBackground.x), (int)(255 * pinBackground.y), (int)(255 * pinBackground.z), outputAlpha),
				4.0f,
				3);
			// ImGui::PushStyleVar(ImGuiStyleVar_AntiAliasFringeScale, 1.0f);
			drawList->AddRect(
				outputsRect.GetTL(),
				outputsRect.GetBR() - ImVec2(0, 1),
				IM_COL32((int)(255 * pinBackground.x), (int)(255 * pinBackground.y), (int)(255 * pinBackground.z), outputAlpha),
				4.0f,
				3);
			// ImGui::PopStyleVar();
			drawList->AddRectFilled(contentRect.GetTL(), contentRect.GetBR(), IM_COL32(24, 64, 128, 200), 0.0f);
			// ImGui::PushStyleVar(ImGuiStyleVar_AntiAliasFringeScale, 1.0f);
			drawList->AddRect(contentRect.GetTL(), contentRect.GetBR(), IM_COL32(48, 128, 255, 100), 0.0f);
			// ImGui::PopStyleVar();
		}

		for (auto &node : s_Nodes) {
			if (node.Type != NodeType::Comment)
				continue;

			const float commentAlpha = 0.75f;

			ImGui::PushStyleVar(ImGuiStyleVar_Alpha, commentAlpha);
			ed::PushStyleColor(ed::StyleColor_NodeBg, ImColor(255, 255, 255, 64));
			ed::PushStyleColor(ed::StyleColor_NodeBorder, ImColor(255, 255, 255, 64));
			ed::BeginNode(node.ID);
			ImGui::PushID(node.ID.AsPointer());
			ImGui::BeginVertical("content");
			ImGui::BeginHorizontal("horizontal");
			ImGui::Spring(1);
			ImGui::TextUnformatted(node.Name.c_str());
			ImGui::Spring(1);
			ImGui::EndHorizontal();
			ed::Group(node.Size);
			ImGui::EndVertical();
			ImGui::PopID();
			ed::EndNode();
			ed::PopStyleColor(2);
			ImGui::PopStyleVar();

			if (ed::BeginGroupHint(node.ID)) {
				// auto alpha   = static_cast<int>(commentAlpha * ImGui::GetStyle().Alpha * 255);
				auto bgAlpha = static_cast<int>(ImGui::GetStyle().Alpha * 255);

				// ImGui::PushStyleVar(ImGuiStyleVar_Alpha, commentAlpha * ImGui::GetStyle().Alpha);

				auto min = ed::GetGroupMin();
				// auto max = ed::GetGroupMax();

				ImGui::SetCursorScreenPos(min - ImVec2(-8, ImGui::GetTextLineHeightWithSpacing() + 4));
				ImGui::BeginGroup();
				ImGui::TextUnformatted(node.Name.c_str());
				ImGui::EndGroup();

				auto drawList = ed::GetHintBackgroundDrawList();

				auto hintBounds      = ImGui_GetItemRect();
				auto hintFrameBounds = ImRect_Expanded(hintBounds, 8, 4);

				drawList->AddRectFilled(
					hintFrameBounds.GetTL(), hintFrameBounds.GetBR(), IM_COL32(255, 255, 255, 64 * bgAlpha / 255), 4.0f);

				drawList->AddRect(
					hintFrameBounds.GetTL(), hintFrameBounds.GetBR(), IM_COL32(255, 255, 255, 128 * bgAlpha / 255), 4.0f);

				// ImGui::PopStyleVar();
			}
			ed::EndGroupHint();
		}

		for (auto &link : s_Links)
			ed::Link(link.ID, link.StartPinID, link.EndPinID, link.Color, 2.0f);

		if (!createNewNode) {
			if (ed::BeginCreate(ImColor(255, 255, 255), 2.0f)) {
				auto showLabel = [](const char *label, ImColor color) {
					ImGui::SetCursorPosY(ImGui::GetCursorPosY() - ImGui::GetTextLineHeight());
					auto size = ImGui::CalcTextSize(label);

					auto padding = ImGui::GetStyle().FramePadding;
					auto spacing = ImGui::GetStyle().ItemSpacing;

					ImGui::SetCursorPos(ImGui::GetCursorPos() + ImVec2(spacing.x, -spacing.y));

					auto rectMin = ImGui::GetCursorScreenPos() - padding;
					auto rectMax = ImGui::GetCursorScreenPos() + size + padding;

					auto drawList = ImGui::GetWindowDrawList();
					drawList->AddRectFilled(rectMin, rectMax, color, size.y * 0.15f);
					ImGui::TextUnformatted(label);
				};


				// Links creation
				ed::PinId startPinId = 0, endPinId = 0;
				if (ed::QueryNewLink(&startPinId, &endPinId)) {
					auto startPin = FindPin(startPinId);
					auto endPin   = FindPin(endPinId);

					newLinkPin = startPin ? startPin : endPin;

					if (startPin->Kind == PinKind::Input) {
						std::swap(startPin, endPin);
						std::swap(startPinId, endPinId);
					}

					// Links Rules between pins
					if (startPin && endPin) {
						if (endPin == startPin) {
							ed::RejectNewItem(ImColor(255, 0, 0), 2.0f);
						}

						// Reject input to output in the same node
						else if ( endPin->Node == startPin->Node){
							showLabel("x Impossible to loop node link", ImColor(45, 32, 32, 180));
							ed::RejectNewItem(ImColor(255, 0, 0), 2.0f);
						}

						// Reject pin auto linkage
						else if (endPin->Kind == startPin->Kind) {
							showLabel("x Incompatible Pin Kind", ImColor(45, 32, 32, 180));
							ed::RejectNewItem(ImColor(255, 0, 0), 2.0f);
						}
						
						// Reject  links between differents type
						// Except FloatManager out -Linkedto- Float in
						// Except ColorManager out -Linkedto- Color in
						else if ( endPin->Type != startPin->Type && 
							! ( endPin->Type == PinType::Float && startPin->Type == PinType::FloatManager )  &&
						 	! ( endPin->Type == PinType::Framebuffer && startPin->Type == PinType::ColorManager ) ){
							showLabel("x Incompatible Pin Type", ImColor(45, 32, 32, 180));
							ed::RejectNewItem(ImColor(255, 128, 128), 1.0f);
						} 

						else {
							showLabel("+ Create Link", ImColor(32, 45, 32, 180));
							if (ed::AcceptNewItem(ImColor(128, 255, 128), 4.0f)) {
								s_Links.emplace_back(Link(GetNextId(), startPinId, endPinId));
								s_Links.back().Color = GetIconColor(startPin->Type);
							}
						}
					}
				}

				ed::PinId pinId = 0;
				if (ed::QueryNewNode(&pinId)) {
					newLinkPin = FindPin(pinId);
					if (newLinkPin)
						showLabel("+ Create Node", ImColor(32, 45, 32, 180));

					if (ed::AcceptNewItem()) {
						createNewNode  = true;
						newNodeLinkPin = FindPin(pinId);
						newLinkPin     = nullptr;
						ed::Suspend();
						ImGui::OpenPopup("Create New Node");
						ed::Resume();
					}
				}
			} else
				newLinkPin = nullptr;

			ed::EndCreate();

			if (ed::BeginDelete()) {

				// Inner managed links deletion
				ed::LinkId linkId = 0;
				while (ed::QueryDeletedLink(&linkId)) {
					if (ed::AcceptDeletedItem()) {
						auto id =
							std::find_if(s_Links.begin(), s_Links.end(), [linkId](auto &link) { return link.ID == linkId; });
						if (id != s_Links.end())
							s_Links.erase(id);
					}
				}

				// Inner managed node deletion
				ed::NodeId nodeId = 0;
				while (ed::QueryDeletedNode(&nodeId)) {
					if (ed::AcceptDeletedItem()) {
						auto id =
							std::find_if(s_Nodes.begin(), s_Nodes.end(), [nodeId](auto &node) { return node.ID == nodeId; });
						if (id != s_Nodes.end())
							s_Nodes.erase(id);
					}
				}
			}
			ed::EndDelete();
		}

		ImGui::SetCursorScreenPos(cursorTopLeft);
	}

#if 1
	auto openPopupPosition = ImGui::GetMousePos();
	ed::Suspend();
	if (ed::ShowNodeContextMenu(&contextNodeId))
		ImGui::OpenPopup("Node Context Menu");
	else if (ed::ShowPinContextMenu(&contextPinId))
		ImGui::OpenPopup("Pin Context Menu");
	else if (ed::ShowLinkContextMenu(&contextLinkId))
		ImGui::OpenPopup("Link Context Menu");
	else if (ed::ShowBackgroundContextMenu()) {
		ImGui::OpenPopup("Create New Node");
		newNodeLinkPin = nullptr;
	}
	ed::Resume();

	ed::Suspend();
	ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, ImVec2(8, 8));
	if (ImGui::BeginPopup("Node Context Menu")) {
		auto node = FindNode(contextNodeId);

		ImGui::TextUnformatted("Node Context Menu");
		ImGui::Separator();
		if (node) {
			ImGui::Text("ID: %p", node->ID.AsPointer());
			ImGui::Text("Type: %s",
						node->Type == NodeType::Blueprint ? "Blueprint" : (node->Type == NodeType::Tree ? "Tree" : "Comment"));
			ImGui::Text("Inputs: %d", (int)node->Inputs.size());
			ImGui::Text("Outputs: %d", (int)node->Outputs.size());
		} else
			ImGui::Text("Unknown node: %p", contextNodeId.AsPointer());
		ImGui::Separator();
		if (ImGui::MenuItem("Delete"))
			ed::DeleteNode(contextNodeId);
		ImGui::EndPopup();
	}

	if (ImGui::BeginPopup("Pin Context Menu")) {
		auto pin = FindPin(contextPinId);

		ImGui::TextUnformatted("Pin Context Menu");
		ImGui::Separator();
		if (pin) {
			ImGui::Text("ID: %p", pin->ID.AsPointer());
			if (pin->Node)
				ImGui::Text("Node: %p", pin->Node->ID.AsPointer());
			else
				ImGui::Text("Node: %s", "<none>");
		} else
			ImGui::Text("Unknown pin: %p", contextPinId.AsPointer());

		ImGui::EndPopup();
	}

	if (ImGui::BeginPopup("Link Context Menu")) {
		auto link = FindLink(contextLinkId);

		ImGui::TextUnformatted("Link Context Menu");
		ImGui::Separator();
		if (link) {
			ImGui::Text("ID: %p", link->ID.AsPointer());
			ImGui::Text("From: %p", link->StartPinID.AsPointer());
			ImGui::Text("To: %p", link->EndPinID.AsPointer());
		} else
			ImGui::Text("Unknown link: %p", contextLinkId.AsPointer());
		ImGui::Separator();
		if (ImGui::MenuItem("Delete"))
			ed::DeleteLink(contextLinkId);
		ImGui::EndPopup();
	}
	

	// Right Click contextual menu
	if (ImGui::BeginPopup("Create New Node")) {
		auto newNodePostion = openPopupPosition;
		Node *node = nullptr;
		
		if (ImGui::MenuItem("3D Scene"))
			node = SpawnSceneNode();
		if (ImGui::MenuItem("3D Render"))
			node = Spawn3DRenderNode();
		if (ImGui::MenuItem("3D To Framebuffer"))
			node = Spawn3DToFrameBufferNode();
		if (ImGui::MenuItem("3D to Rendered Image"))
			node = Spawn3DRenderedImageNode();
		if (ImGui::MenuItem("Framebuffer to Image"))
			node = SpawnRenderedImageNode();

		ImGui::Separator(); // Passes
		if (ImGui::MenuItem("Gaussian Blur"))
			node = SpawnBlurNode();
		

		ImGui::Separator(); // Color Managers
		if (ImGui::MenuItem("Color Vector Manager"))
			node = SpawnColorVectManagerNode();

		ImGui::Separator(); // Natives Data Managers
			if (ImGui::MenuItem("Float"))
				node = SpawnFloatNode();

		ImGui::Separator(); // Comparators
		if (ImGui::MenuItem("Lower Than"))
			node = SpawnLowerBetweenNode();
		if (ImGui::MenuItem("Greater Than"))
			node = SpawnGreaterBetweenNode();

		ImGui::Separator(); // Comments
		if (ImGui::MenuItem("Comment"))
			node = SpawnComment();

		ImGui::Separator(); // Useless ?
			if (ImGui::MenuItem("Weird"))
				node = SpawnWeirdNode();

			if (ImGui::MenuItem("Trace by Channel"))
				node = SpawnTraceByChannelNode();

		if (node) {
			createNewNode = false;

			ed::SetNodePosition(node->ID, newNodePostion);

			if (auto startPin = newNodeLinkPin) {
				auto &pins = startPin->Kind == PinKind::Input ? node->Outputs : node->Inputs;

				for (auto &pin : pins) {
					if (CanCreateLink(startPin, &pin)) {
						auto endPin = &pin;
						if (startPin->Kind == PinKind::Input)
							std::swap(startPin, endPin);

						s_Links.emplace_back(Link(GetNextId(), startPin->ID, endPin->ID));
						s_Links.back().Color = GetIconColor(startPin->Type);

						break;
					}
				}
			}
		}

		ImGui::EndPopup();
	} else
		createNewNode = false;
	ImGui::PopStyleVar();
	ed::Resume();
#endif


	ed::End();
}
