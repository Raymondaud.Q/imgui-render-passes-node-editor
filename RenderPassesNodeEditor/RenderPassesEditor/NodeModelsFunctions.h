#ifndef __NODEMODELSFUNCTIONS_H__
#define __NODEMODELSFUNCTIONS_H__

#include <ax/Builders.h>
#include "NodeModel.h"

extern std::vector<Node> s_Nodes ;
extern int s_NextId ;

void BuildNode(Node *node);

void BuildNodes();

int GetNextId();

Node* SpawnLowerBetweenNode();

Node* SpawnGreaterBetweenNode();

Node* SpawnWeirdNode();

Node* SpawnRenderedImageNode();

Node* Spawn3DRenderedImageNode();

Node* SpawnTraceByChannelNode();

Node* SpawnSceneNode();

Node* Spawn3DRenderNode();

Node* Spawn3DToFrameBufferNode();

Node* SpawnBlurNode();

Node* SpawnFloatNode();

Node* SpawnColorVectManagerNode();

Node* SpawnComment();

#endif