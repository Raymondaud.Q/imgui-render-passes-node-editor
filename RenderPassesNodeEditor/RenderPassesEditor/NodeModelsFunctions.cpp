# include "NodeModelsFunctions.h"

int s_NextId = 1;
std::vector<Node> s_Nodes;

void BuildNode(Node *node)
{
    for (auto &input : node->Inputs) {
        input.Node = node;
        input.Kind = PinKind::Input;
    }

    for (auto &output : node->Outputs) {
        output.Node = node;
        output.Kind = PinKind::Output;
    }
}

void BuildNodes()
{
    for (auto &node : s_Nodes)
        BuildNode(&node);
}

int GetNextId()
{
    return s_NextId++;
}

Node* Spawn3DRenderedImageNode()
{
	s_Nodes.emplace_back(GetNextId(), "Rendered Image", ImColor(255, 128, 200));
    s_Nodes.back().Inputs.emplace_back(GetNextId(), "Color", PinType::Framebuffer);
    s_Nodes.back().Inputs.emplace_back(GetNextId(), "Depth", PinType::Framebuffer);
    s_Nodes.back().Inputs.emplace_back(GetNextId(), "Normal", PinType::Framebuffer);
    s_Nodes.back().Inputs.emplace_back(GetNextId(), "UV coord", PinType::Framebuffer);

    BuildNode(&s_Nodes.back());

    return &s_Nodes.back();
}

Node* SpawnRenderedImageNode()
{
	s_Nodes.emplace_back(GetNextId(), "Rendered Image", ImColor(255, 128, 200));
    s_Nodes.back().Inputs.emplace_back(GetNextId(), "Framebuffer", PinType::Framebuffer);

    BuildNode(&s_Nodes.back());

    return &s_Nodes.back();
}


Node* SpawnLowerBetweenNode()
{
    s_Nodes.emplace_back(GetNextId(), " < Lower", ImColor(128, 195, 248));
    s_Nodes.back().Type = NodeType::Simple;
    s_Nodes.back().Inputs.emplace_back(GetNextId(), "", PinType::Float);
    s_Nodes.back().Inputs.emplace_back(GetNextId(), "", PinType::Float);
    s_Nodes.back().Outputs.emplace_back(GetNextId(), "", PinType::Float);

    BuildNode(&s_Nodes.back());

    return &s_Nodes.back();
}

Node* SpawnGreaterBetweenNode()
{
    s_Nodes.emplace_back(GetNextId(), " > Greater", ImColor(128, 195, 248));
    s_Nodes.back().Type = NodeType::Simple;
    s_Nodes.back().Inputs.emplace_back(GetNextId(), "", PinType::Float);
    s_Nodes.back().Inputs.emplace_back(GetNextId(), "", PinType::Float);
    s_Nodes.back().Outputs.emplace_back(GetNextId(), "", PinType::Float);

    BuildNode(&s_Nodes.back());

    return &s_Nodes.back();
}

Node* SpawnWeirdNode()
{
    s_Nodes.emplace_back(GetNextId(), "o.O", ImColor(128, 195, 248));
    s_Nodes.back().Type = NodeType::Simple;
    s_Nodes.back().Inputs.emplace_back(GetNextId(), "", PinType::Float);
    s_Nodes.back().Outputs.emplace_back(GetNextId(), "", PinType::Float);
    s_Nodes.back().Outputs.emplace_back(GetNextId(), "", PinType::Float);

    BuildNode(&s_Nodes.back());

    return &s_Nodes.back();
}

Node* SpawnTraceByChannelNode()
{
    s_Nodes.emplace_back(GetNextId(), "Single Line Trace by Channel", ImColor(255, 128, 64));
    // s_Nodes.back().Inputs.emplace_back(GetNextId(), "", PinType::Flow);
    // s_Nodes.back().Inputs.emplace_back(GetNextId(), "Start", PinType::Flow);
    s_Nodes.back().Inputs.emplace_back(GetNextId(), "End", PinType::Int);
    s_Nodes.back().Inputs.emplace_back(GetNextId(), "Trace Channel", PinType::Float);
    s_Nodes.back().Inputs.emplace_back(GetNextId(), "Trace Complex", PinType::Bool);
    s_Nodes.back().Inputs.emplace_back(GetNextId(), "Actors to Ignore", PinType::Int);
    s_Nodes.back().Inputs.emplace_back(GetNextId(), "Draw Debug Type", PinType::Bool);
    s_Nodes.back().Inputs.emplace_back(GetNextId(), "Ignore Self", PinType::Bool);
    // s_Nodes.back().Outputs.emplace_back(GetNextId(), "", PinType::Flow);
    s_Nodes.back().Outputs.emplace_back(GetNextId(), "Out Hit", PinType::Float);
    s_Nodes.back().Outputs.emplace_back(GetNextId(), "Return Value", PinType::Bool);

    BuildNode(&s_Nodes.back());

    return &s_Nodes.back();
}

Node* SpawnSceneNode()
{
    s_Nodes.emplace_back(GetNextId(), "3D Scene", ImColor(0, 128, 200));
    s_Nodes.back().Outputs.emplace_back(GetNextId(), "3D scene", PinType::Scene);

    BuildNode(&s_Nodes.back());

    return &s_Nodes.back();
}

Node* Spawn3DRenderNode()
{
    s_Nodes.emplace_back(GetNextId(), "Render", ImColor(255, 128, 200));
    s_Nodes.back().Inputs.emplace_back(GetNextId(), "3D scene", PinType::Scene);
    s_Nodes.back().Outputs.emplace_back(GetNextId(), "Color", PinType::Framebuffer);
    s_Nodes.back().Outputs.emplace_back(GetNextId(), "Depth", PinType::Framebuffer);
    s_Nodes.back().Outputs.emplace_back(GetNextId(), "Normal", PinType::Framebuffer);
    s_Nodes.back().Outputs.emplace_back(GetNextId(), "UV coord", PinType::Framebuffer);

    BuildNode(&s_Nodes.back());

    return &s_Nodes.back();
}

Node* Spawn3DToFrameBufferNode(){
	s_Nodes.emplace_back(GetNextId(), "3D to Framebuffer", ImColor(255, 128, 200));
    s_Nodes.back().Outputs.emplace_back(GetNextId(), "Framebuffer", PinType::Framebuffer);
    s_Nodes.back().Inputs.emplace_back(GetNextId(), "Color", PinType::Framebuffer);
    s_Nodes.back().Inputs.emplace_back(GetNextId(), "Depth", PinType::Framebuffer);
    s_Nodes.back().Inputs.emplace_back(GetNextId(), "Normal", PinType::Framebuffer);
    s_Nodes.back().Inputs.emplace_back(GetNextId(), "UV coord", PinType::Framebuffer);

    BuildNode(&s_Nodes.back());

    return &s_Nodes.back();

}

Node* SpawnBlurNode()
{
    s_Nodes.emplace_back(GetNextId(), "Blur", ImColor(255, 128, 200));
    s_Nodes.back().Inputs.emplace_back(GetNextId(), "Framebuffer", PinType::Framebuffer);
    s_Nodes.back().Inputs.emplace_back(GetNextId(), "Radius", PinType::Float);
    s_Nodes.back().Outputs.emplace_back(GetNextId(), "Framebuffer", PinType::Framebuffer);


    BuildNode(&s_Nodes.back());

    return &s_Nodes.back();
}

Node* SpawnFloatNode()
{
    s_Nodes.emplace_back(GetNextId(), "Float Slider", ImColor(255, 128, 200));
    // ICI s_Nodes.back().Type = NodeType::Float;
    s_Nodes.back().Outputs.emplace_back(GetNextId(), "", PinType::FloatManager);
    s_Nodes.back().floatModel.push_back({});
    // ICI s_Nodes.back().floatModel.back().Name  = "test float";
    s_Nodes.back().floatModel.back().Min   = 0.001f;
    s_Nodes.back().floatModel.back().Max   = 10.f;
    s_Nodes.back().floatModel.back().Value = 1.f;

    BuildNode(&s_Nodes.back());

    return &s_Nodes.back();
}

Node* SpawnColorVectManagerNode()
{
	s_Nodes.emplace_back(GetNextId(), "Color Vector Manager", ImColor(255, 128, 200));
    s_Nodes.back().Inputs.emplace_back(GetNextId(), "Color", PinType::Framebuffer);
    s_Nodes.back().Outputs.emplace_back(GetNextId(), "", PinType::ColorManager);
    s_Nodes.back().floatModel.push_back({});
    s_Nodes.back().floatModel.back().Min   = 0.0f;
    s_Nodes.back().floatModel.back().Max   = 1.f;
    s_Nodes.back().floatModel.back().Value = 0.5f;

    s_Nodes.back().floatModel.push_back({});
	s_Nodes.back().floatModel.back().Min   = 0.0f;
    s_Nodes.back().floatModel.back().Max   = 1.f;
    s_Nodes.back().floatModel.back().Value = 0.5f;

    s_Nodes.back().floatModel.push_back({});
	s_Nodes.back().floatModel.back().Min   = 0.0f;
    s_Nodes.back().floatModel.back().Max   = 1.f;
    s_Nodes.back().floatModel.back().Value = 0.5f;

    BuildNode(&s_Nodes.back());

    return &s_Nodes.back();
}


Node* SpawnComment()
{
    s_Nodes.emplace_back(GetNextId(), "Test Comment");
    s_Nodes.back().Type = NodeType::Comment;
    s_Nodes.back().Size = ImVec2(300, 200);

    return &s_Nodes.back();
}