#ifndef __NODEMODEL_H__
#define __NODEMODEL_H__

#include <string>
#include <vector>

#include <imgui_node_editor.h>
#define IMGUI_DEFINE_MATH_OPERATORS
namespace ed   = ax::NodeEditor;


enum class PinType {

    Bool,
    Int,
    Float,
    FloatManager, // ICI : Ajout type de pin : TODO lui attribuer une forme particulière 
    Scene,
    Framebuffer,
    Texture,
    ColorManager

};

enum class PinKind { Output, Input };

enum class NodeType {

    Simple,
    Blueprint,
    Comment,
    Tree,
    Float

};

struct Node;

struct Pin {

    ed::PinId ID;
    ::Node *Node;
    std::string Name;
    PinType Type;
    PinKind Kind;

    Pin(int id, const char *name, PinType type) : ID(id), Node(nullptr), Name(name), Type(type), Kind(PinKind::Input) {}
};

struct FloatModel {

    std::string Name;
    float Value;
    float Min;
    float Max;
};

struct Node {

    ed::NodeId ID;
    std::string Name;
    std::vector<Pin> Inputs;
    std::vector<Pin> Outputs;
    std::vector<FloatModel> floatModel;
    ImVec4  ColorPicker;
    ImColor Color;
    NodeType Type;
    ImVec2 Size;

    std::string State;
    std::string SavedState;

    Node(int id, const char *name, ImColor color = ImColor(255, 255, 255))
        : ID(id), Name(name), Color(color), Type(NodeType::Blueprint), Size(0, 0) {}
};

struct Link {

    ed::LinkId ID;
    ed::PinId StartPinID;
    ed::PinId EndPinID;
    ImColor Color;

    Link(ed::LinkId id, ed::PinId startPinId, ed::PinId endPinId)
        : ID(id), StartPinID(startPinId), EndPinID(endPinId), Color(255, 255, 255) {}
};


#endif