# TODO

* [Typo refactor](https://fr.wiktionary.org/wiki/Wiktionnaire:Conventions_typographiques)
* Link & Node parsing for rendering 
And maybe pass rendering 

# Render Passes Editor

This C++ render pass editor is made from a fork of [Dear ImGui Node Editor ](https://github.com/thedmd/imgui-node-editor).

# Dependencies & Setup on Debian-based OS

Of course, you need a [C++ SDK](https://packages.debian.org/sid/build-essential) :
* sudo apt install build−essential

A [font configuration](https://packages.debian.org/sid/libfontconfig1) and customization library : 
* sudo apt−get install libfontconfig1

The [OpenGL developper extensions](https://packages.debian.org/jessie/mesa-common-dev) library :
* sudo apt−get install mesa−common−dev

The [GL3W](https://github.com/skaslev/gl3w)  rendering interface bindings :
* sudo apt-get update && sudo apt-get install libglfw3-dev


# Running the application

You can run this application by simply add the execution right to [LAUNCHER.sh](https://gitlab.com/CERI_Raymondaud.Q/nodeeditor/-/blob/master/LAUNCHER.sh) and run it by typing "./LAUNCHER.sh" in a terminal opened in the same directory.

However you can also look at the content of this file to DIY.

# HOC Architecture
[Render Passes Node Editor](https://gitlab.com/CERI_Raymondaud.Q/imgui-render-passes-node-editor/-/tree/master/RenderPassesNodeEditor/RenderPassesEditor)
```
|
│ Data ( Forked )
| NodeEditor ( Forked )
| Support ( Forked )
│ ThirdParty ( Forked )
| CMakeLists.txt ( Forked )
| Licence ( Forked )
│ LAUNCHER.sh
|
└───  Examples
|           |
|           | Canvas ( Forked )
|           | Common ( Forked)
|           | BasicInteraction ( Forked )
|           | Blueprints ( Forked )
|           | Simple ( Forked )
|           |
|           └───  [RenderPassesEditor  (HOC)]
|                       |
|                       |   CMakeLists.txt
|                       |   NodeModel.h
|                       |   NodeModelsFunctions.h
|                       |   NodeModelsFunctions.cpp
|                       |   RenderPassesEditor.cpp
| END
```
